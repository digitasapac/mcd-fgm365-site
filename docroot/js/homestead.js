const app = document.getElementById('root')
const container = document.createElement('div')
container.setAttribute('class', 'container')
app.appendChild(container)

var request = new XMLHttpRequest()
request.open('GET', 'http://api.homestead.local/api/v1/authors', true)
request.onload = function () {

  var data = JSON.parse(this.response)
  if (request.status >= 200 && request.status < 400) {
    data.forEach((author) => {
      const card = document.createElement('div')
      card.setAttribute('class', 'card')

      const h1 = document.createElement('h1')
      h1.textContent = `AUTHOR ID: ${author.id}`

      const p1 = document.createElement('p')
      p1.textContent = `AUTHOR NAME : ${author.name}...`

      const p2 = document.createElement('p')
      p2.textContent = `AUTHOR EMAIL : ${author.email}...`

      container.appendChild(card)
      card.appendChild(h1)
      card.appendChild(p1)
      card.appendChild(p2)
    })
  } else {
    const errorMessage = document.createElement('marquee')
    errorMessage.textContent = `Gah, it's not working!`
    app.appendChild(errorMessage)
  }
}

request.send()